#include "gearing.h"


static fgerrno_t fgerrno = 0;

fgerrno_t fg_error() {
	return fgerrno;
}


static unsigned int __mode = FG_MODE_BALANCED;
fgbool_t fg_set_mode(unsigned int mode) {
	if (mode > 5) {
		fgerrno = FG_ERR_PARAMS;
		return fgfalse;
	}
	__mode = mode;
	return fgtrue;
}
unsigned int fg_get_mode() {
	return __mode;
}

static fgfloat_t __mode_manual = 0.0;
fgbool_t fg_set_mode_manual(fgfloat_t ratiomuladd) {
	if (__mode != FG_MODE_MANUAL) {
		fgerrno = FG_ERR_PARAMS;
		return fgfalse;
	}
	__mode_manual = ratiomuladd;
	return fgtrue;
}
fgfloat_t fg_get_mode_manual() {
	return __mode_manual;
}

fgbool_t fg_get_ratio(fgfloat_t *result, fgfloat_t first, fgfloat_t second) {
	if (!result) {
		fgerrno = FG_ERR_NULL;
		return fgfalse;
	}
	if (first <= second) {
		fgerrno = FG_ERR_PARAMS;
		return fgfalse;
	}
	*result = (1.0 - (second / first));
	return fgtrue;
}

fgbool_t fg_calculate_ex(fgresult_t *result, fgfloat_t first, fgfloat_t ratio) {
	if (!result) {
		fgerrno = FG_ERR_NULL;
		return fgfalse;
	}

	result->gears[0] = first;

	fgfloat_t tmp, ratiomul;

	switch (__mode) {
		case FG_MODE_ACCELERATION:
			ratiomul = 0.78;
			break;
		case FG_MODE_ACCELERATION_BALANCED:
			ratiomul = 0.79;
			break;
		case FG_MODE_SPEED:
			ratiomul = 0.85;
			break;
		case FG_MODE_SPEED_BALANCED:
			ratiomul = 0.825;
			break;
		case FG_MODE_MANUAL:
			ratiomul = __mode_manual;
			break;
		default:
			ratiomul = 0.8;
			break;
	}

	unsigned short i;
	for (i = 1; i < FG_NUM_GEARS; ++i) {
		tmp = result->gears[i - 1];
		tmp = tmp - (tmp * ratio);
		result->gears[i] = tmp;
		ratio *= ratiomul;
	}

	return fgtrue;
}

fgbool_t fg_calculate(fgresult_t *result, fgfloat_t first, fgfloat_t second) {
	if (!result) {
		fgerrno = FG_ERR_NULL;
		return fgfalse;
	}

	fgfloat_t ratio;
	if (!fg_get_ratio(&ratio, first, second)) {
		return fgfalse;
	}

	return fg_calculate_ex(result, first, ratio);
}
