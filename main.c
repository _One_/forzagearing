#include <stdio.h>
#include "gearing.h"

int main(int argc, char *argv[], char *env[]) {
	fgfloat_t first, second, ratiomul;
	unsigned int mode = 0;

	printf("Enter first gear value: ");
	scanf("%f", &first);

	printf("Enter second gear value: ");
	scanf("%f", &second);

	printf("\n[%u] Balanced\n[%u] Manual\n[%u] Acceleration\n[%u] Acceleration Balanced\n[%u] Speed\n[%u] Speed Balanced\nPick a mode: ", FG_MODE_BALANCED, FG_MODE_MANUAL, FG_MODE_ACCELERATION, FG_MODE_ACCELERATION_BALANCED, FG_MODE_SPEED, FG_MODE_SPEED_BALANCED);
	scanf("%u", &mode);

	if (!fg_set_mode(mode)) {
		goto __fg_exit;
	}
	if (mode == FG_MODE_MANUAL) {
		printf("Enter ratio multiplication: ");
		scanf("%f", &ratiomul);
		if (!fg_set_mode_manual(ratiomul)) {
			goto __fg_exit;
		}
	}
	printf("\nSelected mode is %u (%s)\n", mode, ((mode == FG_MODE_BALANCED) ? "balanced" : ((mode == FG_MODE_MANUAL) ? "manual" : ((mode == FG_MODE_ACCELERATION) ? "acceleration" : ((mode == FG_MODE_ACCELERATION_BALANCED) ? "acceleration balanced" : ((mode == FG_MODE_SPEED) ? "speed" : ((mode == FG_MODE_SPEED_BALANCED) ? "speed balanced" : "unknown")))))));

	fgfloat_t ratio;
	if (!fg_get_ratio(&ratio, first, second)) {
		goto __fg_exit;
	}
	printf("Gear starting ratio is %f\n\n", ratio);

	fgresult_t result;
	if (fg_calculate(&result, first, second)) {
		unsigned short i;
		for (i = 0; i < FG_NUM_GEARS; ++i) {
			printf("Gear %u should be %.2f\n", (i + 1), result.gears[i]);
		}
	}

__fg_exit:;
	fgerrno_t error = fg_error();
	if (error) {
		printf("Error: %u\n", error);
		return 1;
	}

	return 0;
}
