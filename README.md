Simple program to calculate somewhat proper gearing values when tuning cars in Forza (Horizon 2).

If one mode doesn't work for a car, try another or enter a ratio multiplication manually (0.70-0.90).

Tested on:

* ```FH2``` **Audi** RS 3 SportBack **[0.78]**
* ```FH2``` **Bugatti** Veyron SuperSport **[0.85]**
* ```FH2``` **Ferrari** LaFerrari **[0.80]**
* ```FH2``` **Ford** F-150 SVT Raptor **[0.825]**
* ```FH2``` **Lamborghini** Huracan LP 610-4 **[0.78]**
* ```FH2``` **Toyota** Supra Fast & Furious Edition **[0.85]**
* ```FH2``` **Ultima** GTR **[0.85]**