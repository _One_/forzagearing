#ifndef __F_GEARING_H
#define __F_GEARING_H


#define FG_NUM_GEARS					7

#define FG_ERR_NULL						1
#define FG_ERR_PARAMS					2

#define FG_MODE_BALANCED				0
#define FG_MODE_MANUAL					1
#define FG_MODE_ACCELERATION			2
#define FG_MODE_ACCELERATION_BALANCED	3
#define FG_MODE_SPEED					4
#define FG_MODE_SPEED_BALANCED			5

typedef unsigned short	fgerrno_t;

typedef float			fgfloat_t;

typedef unsigned char	fgbool_t;
#define fgtrue			1
#define fgfalse			0

typedef struct fgresult {
	fgfloat_t gears[FG_NUM_GEARS];
} fgresult_t;


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

fgerrno_t fg_error();
fgbool_t fg_set_mode(unsigned int mode);
unsigned int fg_get_mode();
fgbool_t fg_set_mode_manual(fgfloat_t ratiomuladd);
fgfloat_t fg_get_mode_manual();
fgbool_t fg_get_ratio(fgfloat_t *result, fgfloat_t first, fgfloat_t second);
fgbool_t fg_calculate_ex(fgresult_t *result, fgfloat_t first, fgfloat_t ratio);
fgbool_t fg_calculate(fgresult_t *result, fgfloat_t first, fgfloat_t second);

#ifdef __cplusplus
}
#endif // __cplusplus


#endif // __F_GEARING_H
